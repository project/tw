<?php
/**
 * Implementation of hook_default_view_views().
 */
function tw_views_default_views() {
  $views = array();

  // Go through each import table and create default views for them
  $sql = "SELECT twtid,tablename
          FROM {tw_tables}";
  $tblresult = db_query($sql);
  while ($tblrow = db_fetch_object($tblresult)) {
    $twtid = $tblrow->twtid;
    $tablename = $tblrow->tablename;
    // Create a basic table view, with exclusion flags, for each import table
    $view = new view;
    $view->name = "$tablename";
    $view->description = $tablename;
    $view->tag = 'tw';
    $view->view_php = '';
    $view->base_table = $tablename;
    $view->is_cacheable = FALSE;
    $view->api_version = 2;
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
    $handler = $view->new_display('default', $tablename, 'default');
    
    // Grab the available columns for this table (skip any marked ignore, or with no data)
    $pk = NULL;
    $colnames = array();
    $sql = "SELECT colname, primarykey
            FROM {tw_columns}
            WHERE ignorecol=0 AND isempty=0 AND twtid=%d
            ORDER BY weight";
    $colresult = db_query($sql, $twtid);
    while ($colrow = db_fetch_object($colresult)) {
      $colnames[] = $colrow->colname;
      if ($colrow->primarykey) {
        $pk = $colrow->colname;
      }
    }
    // Add our columns to the fields
    $fields = array();
    foreach ($colnames as $colname) {
      $fields[$colname] = array(
        'id' => $colname,
        'table' => $tablename,
        'field' => $colname,
        'label' => $colname,
        'exclude' => 0,
        'relationship' => 'none',
      );
    }
    $handler->override_option('fields', $fields);
    $handler->override_option('access', array(
      'type' => 'perm',
      'perm' => TW_ACCESS,
    ));
    $handler->override_option('title', t('Contents of !tablename', array('!tablename' => $tablename)));
    $handler->override_option('header', t('This is a view of a raw data table in the Drupal databse. It may be '.
      'sorted in various ways by clicking the column headers. 
    
      If you identify a particular field (column) that does not need to be used in views of this table, '.
      'go to the <a href="/admin/content/tw/sources/analyze/!tablename">analysis page</a> '.
      'and check the <em>Ignore</em> box for that field. It will then no longer appear here.',
      array('!tablename' => $tablename)));
    $handler->override_option('header_format', '1');
    $handler->override_option('header_empty', 0);
    $handler->override_option('empty', 'There are no rows in this table.');
    $handler->override_option('empty_format', '1');
    $handler->override_option('items_per_page', 25);
    $handler->override_option('use_pager', '1');
    $handler->override_option('style_plugin', 'table');
    // Add our columns into the style options
    $columns = array();
    foreach ($colnames as $colname) {
      $columns[$colname] = $colname;
    }
    $info = array();
    foreach ($colnames as $colname) {
      $info[$colname] = array(
        'sortable' => 1,
        'separator' => '',
      );
    }
    $handler->override_option('style_options', array(
      'grouping' => '',
      'override' => 1,
      'sticky' => 1,
      'order' => 'asc',
      'columns' => $columns,
      'info' => $info,
      'default' => -1,
    ));
    $handler = $view->new_display('page', 'Page', 'page_1');
    $handler->override_option('path', "admin/content/tw/sources/view/$tablename");
    $handler->override_option('menu', array(
      'type' => 'none',
      'title' => '',
      'weight' => 0,
    ));
    $handler->override_option('tab_options', array(
      'type' => 'none',
      'title' => '',
      'weight' => 0,
    ));
    $views[$view->name] = $view;
  }
  return $views;
}
