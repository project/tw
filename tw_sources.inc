<?php 

/**
 * @file
 * Implementation of source handling
 */

/**
 * Add a fieldset into the import form, for identifying where to obtain source data.
 * 
 * 
 */
function tw_tw_form() {
  ////////////////////////////////////////////////////////////
  // Attach MySQL tables

  // Build list of our own imported and generated tables
  $excludes = array();
  $sql = "SELECT * FROM {tw_tables}";
  $tblresult = db_query($sql);
  while ($tblrow = db_fetch_object($tblresult)) {
    $tblname = $tblrow->tablename;
    // Exclude the table itself...
    $excludes[$tblname] = $tblname;
  }

  $sql = "SHOW TABLES";
  $result = db_query($sql);
  $options = array();
  while ($row = db_fetch_array($result)) {
    foreach ($row as $tablename) {
      if (!$excludes[$tablename]) {
        $options[$tablename] = $tablename;
      }
    }
  }
  $fieldsets['mysql'] = array(
    '#type' => 'fieldset',
    '#title' => t('Attach MySQL tables'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    'uploadhelp' => array(
      '#prefix' => '<div>',
      '#value' => t('Attach tables which have been directly imported to MySQL by other means
        to the Table Wizard module.'),
      '#suffix' => '</div>',
    ),
    'tablelist' => array(
      '#type' => 'select',
      '#title' => t('Non-Drupal tables in the database'),
      '#options' => $options,
      '#description' => t('Check any tables which are to be used as sources for migration
        into Drupal objects.'),
    ),
    'mysqlsubmit' => array(
      '#type' => 'submit',
      '#value' => t('Attach tables'),
     ),
  );
  return $fieldsets;
}

function tw_tw_form_submit_mysql($values) {
  $tableset = array();
  $tablename = $values['tablelist'];
  if ($tablename) {
    $tables = array();
    $tables['tablename'] = $tablename;
    $schema = array();
    $sql = "DESCRIBE $tablename";
    $result = db_query($sql);
    while ($row = db_fetch_object($result)) {
      $field = $row->Field;
      $schema['fields'][$field] = array(
        'posstype' => $row->Type,
        'pk' => ($row->Key == 'PRI'),
      );
    }
    $tables['schema'] = $schema;
    $tableset[] = $tables;
  }
  return $tableset; 
}
   