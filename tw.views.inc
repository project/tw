<?php

/*
 * TODO: Hiding of secure fields
 * TODO: Table names > 32 characters
 */
function tw_views_data() {
  $tables = array();
  
  // Create table definitions for each import table
  $sql = "SELECT * FROM {tw_tables}";
  $tblresult = db_query($sql);
  while ($tblrow = db_fetch_object($tblresult)) {
    $tablename = $tblrow->tablename;
    $twtid = $tblrow->twtid;
    $table = array();
    // Add each viewable column to the table definition
    $sql = "SELECT twcid, colname, primarykey, secure, coltype
            FROM {tw_columns} 
            WHERE twtid=%d AND ignorecol=0 AND isempty=0
            ORDER BY weight";
    $colresult = db_query($sql, $twtid);
    $pk = NULL;
    while ($colrow = db_fetch_object($colresult)) {
      $colname = $colrow->colname;
      $table[$colname] = array(
        'title' => $colname,
        'help' => $colname,
        // TODO: Truncate text at 80 characters.
        // TODO: Better yet, do some jQuery magic to expand to the full text
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'filter' => array(
          'handler' => _tw_views_handler_type($colrow->coltype),
          'allow empty' => TRUE,
        ),
        //'argument'
        'sort' => array(
          'handler' => 'views_handler_sort'
        ),
      );
      if ($colrow->primarykey) {
        $pk = $colname;
      }
    }
    // Any table can be a base table
    $table['table'] = array(
      'group' => t($tablename),
      'base' => array(
        'field' => $pk,
        'title' => t('!tablename', array('!tablename' => $tablename)),
        'help' => t('Data available for migration into Drupal content'),
        'weight' => 10,
      ),
    );
    $tables[$tablename] = $table;
  }
  // Now that all tables are present, fill in relationships defined by foreign keys
  $sql = "SELECT twt1.tablename tbl1, twc1.colname col1, twt2.tablename tbl2, twc2.colname col2
          FROM {tw_relationships} twr
          INNER JOIN {tw_columns} twc1 ON twr.leftcol=twc1.twcid
          INNER JOIN {tw_tables} twt1 ON twc1.twtid=twt1.twtid
          INNER JOIN {tw_columns} twc2 ON twr.rightcol=twc2.twcid
          INNER JOIN {tw_tables} twt2 ON twc2.twtid=twt2.twtid";
  $result = db_query($sql);
  // To allow multiple joins from one tbl/col, must use an alias and 
  // 'relationship field' for the left side
  $i = 0;
  while ($row = db_fetch_array($result)) {
    extract($row);
    if (!isset($tables[$tbl1][$col1]['relationship'])) {
      $tables[$tbl1][$col1]['title'] = t("$col1 (joins to $tbl2)");
      $tables[$tbl1][$col1]['relationship'] = array(
        'base' => $tbl2,
        'base field' => $col2,
        'label' => t("Join $tbl1 to $tbl2"),
      );
    } else {
      $i++;
      $mungedcol = $col1.'_'.$i;
      $tables[$tbl1][$mungedcol] = $tables[$tbl1][$col1];
      $tables[$tbl1][$mungedcol]['title'] = t("$col1 (joins to $tbl2)");
      $tables[$tbl1][$mungedcol]['real field'] = $col1;
      $tables[$tbl1][$mungedcol]['relationship'] = array(
        'base' => $tbl2,
        'base field' => $col2,
        'relationship field' => $col1,
        'label' => t("Join $tbl1 to $tbl2"),
      );
    }
  }
  return $tables;
}

function _tw_views_handler_type($sqltype) {
  preg_match('/^[a-zA-Z]+/', $sqltype, $matches);
  $type = tw_column_type($matches[0]);
  switch ($type) {
    case 'numeric':
      $filter = 'views_handler_filter_numeric';
      break;
    case 'datetime':
      $filter = 'views_handler_filter_date';
      break;
    default:
      $filter = 'views_handler_filter_string';
      break;
  }

  return $filter;
}
